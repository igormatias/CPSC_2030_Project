<?php
    require_once './config/db/db_connection.php';
    require_once './sendEmail.php';
    
    
    //Global variables!
    $index['nav'] = 'menu_top.twig';
    $index['main'] = 'index.no_side_menu.twig';
    $index['sidemenu'] = 'sidemenu.html.twig';
    $index['body'] = 'front_desk.html.twig';
    $menu_top['loginButton'] = 'menu_top.login.html.twig';
    $user['profile']['email'] = '';
    $menu_top['login']['link'] = 'index.php?action=login';
    $menu_top['login']['label'] = 'login';
    $menu_top['login']['form'] = 'login.html.twig';

    function set_session($user, $email, $keep){
        if ($keep == null) $keep = 'off';
        
        $_SESSION['user_session'] = array(  'user_ID' => $user['user_ID'],
                                            'email' => $email, 
                                            'keep' => $keep,
                                            'time' => time(),
                                            'key' => 1);
    }

    function get_session(){
        return $_SESSION['user_session'];
    }

    function clear_session(){
        $_SESSION['user_session'] = array();
    }

    function is_valid_session(){
        if (isset($_SESSION['user_session'])){
            $session = get_session();
            if ($session != null){
                if ($session['keep'] == 'on' ){
                    $_SESSION['user_session']['time'] = time();
                    return true;
                }
                else{
                    if(($session['time'] + 300) > time()){
                        $_SESSION['user_session']['time'] = time();
                        return true;
                    }
                    return false;
                }
            }
            return false;    
        }
        return false;
    }
    
    function save_to_database($query){
        $conn = connectToDatabase();
        $data = NULL;
        if ($result = $conn->query($query, MYSQLI_USE_RESULT )){
            $row = $result->fetch_all(MYSQLI_NUM);                //returns an array of arrays
            $data = array( 'user_ID' => $row[0][0], 'email_key' => $row[0][1]);
        }
        else{
            $data = NULL;
        }
        return $data;
    }        

    function get_results($query){
        $conn = connectToDatabase();
        //$query = mysqli_real_escape_string($conn, $query);
        $result = $conn->query($query);
        $row = $result->fetch_all(MYSQLI_ASSOC);
        mysqli_free_result($result);
        mysqli_close($conn);
        return $row;
    }

    function get_results_as_num($query){
        $conn = connectToDatabase();
        //$query = mysqli_real_escape_string($conn, $query);
        $result = $conn->query($query);
        $row = $result->fetch_all(MYSQLI_NUM);
        mysqli_free_result($result);
        mysqli_close($conn);
        return $row;
    }


    function create_new_user($email, $password, $subscribe){
        $query = "call create_new_user('$email', '$password', '$subscribe')";
        return save_to_database($query);
    }

    function create_new_user_address($user_id, $address){
        $conn = connectToDatabase();
        $user_id = mysqli_real_escape_string($conn, $user_id);
        //$address = mysqli_real_escape_string($conn, $address);
        $query = "call create_new_user_address($user_id, '$address[0]', '$address[1]', '$address[2]', '$address[3]', '$$address[4]', '$address[5]', '$address[6]', '$address[7]')";
        $result = $conn->query($query);
        mysqli_close($conn);
        return $result;
    }

    function create_new_user_luggage($user_id, $luggage){
        $conn = connectToDatabase();
        $user_id = mysqli_real_escape_string($conn, $user_id);
        //$address = mysqli_real_escape_string($conn, $address);
        $query = "call create_new_user_luggage($user_id, '$luggage[0]', '$luggage[1]', '$luggage[2]')";
        $result = $conn->query($query);
        mysqli_close($conn);
        return $result;

    }

    function send_email($user, $email){
        sendEmail($email, $user['email_key']);
    }

    function check_passkey($token){
        $query = "call check_passkey('$token')";
        return get_results($query)[0];
    }

    function login($email, $password){
        $query = "call check_user('$email', '$password')";
        $result = get_results($query);
        if (count($result) == 0)
            return NULL;
        else
            return $result[0];
    }

    function email_checked($user){
        $user_id = $user['user_ID'];
        $query = "call email_checked('$user_id')";
        return get_results_as_num($query)[0][0];
    }

    function country_list(){
        $query = "call country_list()";
        return get_results_as_num($query);
    }

    function get_user_address_list($user){
        //print_r($user);
        $user_id = $user['user_ID'];
        $query = "call get_user_address_list('$user_id')";
        return get_results_as_num($query);
    }

    function get_user_luggage_offers($user_id){
        //print_r($user);
        $query = "call get_user_luggage_offers('$user_id')";
        return get_results_as_num($query);
    }

    function wrapIntoHTML($tagStart, $tagEnd,$value) {
        return "<$tagStart>$value</$tagEnd>";
    }

    function createSelectBox($list, $name, $class){
        //$ret = "<select size=\"10\" id=\"$name\" class=\"$class\" onchange=\"$name(this.value)\">";
        $ret = "<select id=\"$name\" class=\"$class\">";
        $ret = $ret . wrapIntoHTML("option value=0", "option", "Choose...");
        foreach($list as $item){
            $ret = $ret . wrapIntoHTML("option value=$item[0]", "option", $item[1]);
        }

        $ret = $ret . "</select>";
        return $ret;
    }
    function createSelectBoxCountryList($name, $class){
        $list = country_list();
        return createSelectBox($list, $name, $class);
    }

    function createSelectBoxAddressList($name, $class, $user){
        $list = get_user_address_list($user);
        $address_list = array();
        $i = 0;
        foreach ($list as $address){
            $address_list[$i][0] = $address[0];
            $address_list[$i][1] = $address[1];
            $address_list[$i][1] .= ': ';
            $address_list[$i][1] .= $address[2];
            $address_list[$i][1] .= ', ';
            $address_list[$i][1] .= $address[3];
            $address_list[$i][1] .= '. ';
            $address_list[$i][1] .= $address[4];
            $address_list[$i][1] .= ' - ';
            $address_list[$i][1] .= $address[5];
            $address_list[$i][1] .= ' - ';
            $address_list[$i][1] .= $address[6];
            $address_list[$i][1] .= '. ';
            $address_list[$i][1] .= $address[7];
            $i++;
        }
        return createSelectBox($address_list, $name, $class);
    }

?>