USE Project;

/* Creates a new user without a password, creates a passkey to be sent by email in order to check the email */
DROP PROCEDURE IF EXISTS create_new_user;
DELIMITER //
CREATE PROCEDURE create_new_user(IN email VARCHAR(100), IN password VARCHAR(50), IN subscribe VARCHAR(1))
BEGIN
    DECLARE user_ID INTEGER(10);
    DECLARE passkey VARCHAR(128);
    DECLARE salt VARCHAR(25);
    DECLARE temp VARCHAR(1);
  	DECLARE LCV tinyint;
	DECLARE CTime DATETIME;

	SET @CTime = NOW();
    SET @LCV = 1;

  	SET @temp = ROUND((RAND() * 94) + 32 , 3);
	SET @salt = CHAR(@temp);
    
    WHILE @LCV < 25 DO
  		SET @temp = ROUND((RAND() * 94.0) + 32, 3);
		SET @salt = CONCAT(CHAR(@temp), @salt);
 		SET @LCV = @LCV + 1;
    END WHILE;
    
    INSERT INTO USER(USER.email, USER.password, USER.salt, USER.subscribe) VALUES(email, SHA2(CONCAT(password, @salt), 512), @salt, subscribe);
    SELECT USER.user_ID INTO @user_ID 
    FROM USER 
    WHERE USER.email = email;
    
    INSERT INTO EMAIL_CHECK(user_ID, passkey) 
    VALUES(@user_ID, (SELECT SHA2(CONCAT(email, CURRENT_TIMESTAMP), 512)));
    SELECT EMAIL_CHECK.passkey INTO @passkey 
    FROM EMAIL_CHECK 
    WHERE EMAIL_CHECK.user_ID = @user_ID;

    SELECT @user_ID as 'user_ID', @passkey as 'passkey';
    
END //
DELIMITER ;


/* Adds a password to an existing user, this password is encrypted using SHA-2.512 */
DROP PROCEDURE IF EXISTS update_password;
DELIMITER //
CREATE PROCEDURE update_password(IN ID INTEGER(10), IN password VARCHAR(50))
BEGIN
    DECLARE salt VARCHAR(25);
    DECLARE temp VARCHAR(1);
  	DECLARE LCV tinyint;
	DECLARE CTime DATETIME;

	SET @CTime = NOW();
    SET @LCV = 1;

  	SET @temp = ROUND((RAND() * 94) + 32 , 3);
	SET @salt = CHAR(@temp);
    
    WHILE @LCV < 25 DO
  		SET @temp = ROUND((RAND() * 94.0) + 32, 3);
		SET @salt = CONCAT(CHAR(@temp), @salt);
 		SET @LCV = @LCV + 1;
    END WHILE;
    
    UPDATE USER 
    SET USER.password = SHA2(CONCAT(password, @salt), 512)
    WHERE USER.user_ID = ID;
END //
DELIMITER ;

/* Updates the user information */
DROP PROCEDURE IF EXISTS update_user;
DELIMITER //
CREATE PROCEDURE update_user(IN ID INTEGER(10), IN name VARCHAR(50), IN last_name VARCHAR(50), IN birthdate DATE)
BEGIN
    UPDATE USER 
    SET USER.name = name, 
    USER.last_name = last_name, 
    USER.birthdate = birthdate 
    WHERE USER.user_ID = ID;
END //
DELIMITER ;

/* Check if user email and password matches */ 
DROP PROCEDURE IF EXISTS check_user;
DELIMITER //
CREATE PROCEDURE check_user(IN email VARCHAR(100), IN password VARCHAR(50))
BEGIN
    DECLARE salt VARCHAR(25);
    DECLARE saltedpassword VARCHAR(128);

    SELECT USER.salt into @salt
    FROM USER 
    WHERE USER.email = email;    

    set @saltedpassword = SHA2(CONCAT(password, @salt), 512);

    SELECT USER.user_ID
    FROM USER 
    WHERE USER.email = email AND USER.password = @saltedpassword;
END //
DELIMITER ;

/* check the passkey */ 
DROP PROCEDURE IF EXISTS check_passkey;
DELIMITER //
CREATE PROCEDURE check_passkey(IN passkey VARCHAR(128))
BEGIN   
    DECLARE user_ID    INTEGER(10);
    DECLARE checked_on TIMESTAMP;
    SELECT EMAIL_CHECK.user_ID, EMAIL_CHECK.checked_on into @user_ID, @checked_on
    FROM EMAIL_CHECK 
    WHERE EMAIL_CHECK.passkey = passkey;

    IF @checked_on is null THEN UPDATE EMAIL_CHECK
    SET checked_on = 
    CASE 
        WHEN EMAIL_CHECK.passkey = passkey
        THEN CURRENT_TIMESTAMP
        ELSE NULL
    END
    WHERE EMAIL_CHECK.user_ID = @user_ID;
    ELSE
    SELECT @checked_on as 'checked_on';
    END IF;

    SELECT @user_ID as 'user_ID', (SELECT email FROM USER WHERE USER.user_ID = @user_ID) as 'email';
END //
DELIMITER ;

/* check if the user email was checked */ 
DROP PROCEDURE IF EXISTS email_checked;
DELIMITER //
CREATE PROCEDURE email_checked(IN ID INTEGER(10))
BEGIN
    SELECT CASE
        WHEN EMAIL_CHECK.checked_on IS NOT NULL
        THEN 'Y'
        ELSE 'N'
    END
    FROM EMAIL_CHECK
    WHERE EMAIL_CHECK.user_ID = ID;
END //
DELIMITER ;


DROP PROCEDURE IF EXISTS country_list;
DELIMITER //
CREATE PROCEDURE country_list()
BEGIN
  SELECT *
  FROM COUNTRY;
END //
DELIMITER ;


DROP PROCEDURE IF EXISTS get_user_address_list;
DELIMITER //
CREATE PROCEDURE get_user_address_list(IN ID INTEGER(10))
BEGIN
  SELECT A.address_ID, address_name, address_1, address_2, state, zip, city, country_ID
  FROM ADDRESS A
  INNER JOIN USER_ADDRESS U_A ON A.address_ID = U_A.address_ID
  WHERE U_A.user_ID = ID;
END //
DELIMITER ;


DROP PROCEDURE IF EXISTS create_new_user_address;
DELIMITER //
CREATE PROCEDURE create_new_user_address(IN ID INTEGER(10), IN address_name VARCHAR(20), IN country_ID VARCHAR(2), IN address_1 VARCHAR(300), 
                                         IN address_2 VARCHAR(300), IN city VARCHAR(100), IN state VARCHAR(100), IN zip VARCHAR(20),  IN home VARCHAR(1))
BEGIN
    DECLARE address_ID    INTEGER(10);

    INSERT INTO ADDRESS (
        ADDRESS.address_name, 
        ADDRESS.country_ID, 
        ADDRESS.address_1, 
        ADDRESS.address_2, 
        ADDRESS.city, 
        ADDRESS.state, 
        ADDRESS.zip, 
        ADDRESS.home)
        VALUES (
            address_name, 
            country_ID, 
            address_1,
            address_2,
            city,
            state,
            zip,
            home
            );
    
    SELECT LAST_INSERT_ID() into @address_ID
    FROM ADDRESS
    LIMIT 1;

    INSERT INTO USER_ADDRESS VALUES (ID, @address_ID);
END //
DELIMITER ;


DROP PROCEDURE IF EXISTS create_new_user_luggage;
DELIMITER //
CREATE PROCEDURE create_new_user_luggage(IN ID INTEGER(10), IN country_from VARCHAR(2), IN country_to VARCHAR(2), IN address_ID INTEGER(10))
BEGIN
    DECLARE luggage_ID    INTEGER(10);

    INSERT INTO LUGGAGE (LUGGAGE.country_from, LUGGAGE.country_to, LUGGAGE.address_id)
    VALUES (country_from, country_to, address_ID);

    SELECT LAST_INSERT_ID() into @luggage_ID
    FROM LUGGAGE
    LIMIT 1;
    
    INSERT INTO USER_LUGGAGE VALUES (ID, @luggage_ID);

END //
DELIMITER ;

DROP PROCEDURE IF EXISTS get_user_luggage_list;
DELIMITER //
CREATE PROCEDURE get_user_luggage_list(IN ID INTEGER(10))
BEGIN
  SELECT L.create_on, 
  (SELECT value FROM COUNTRY WHERE country_id = L.country_from), 
  (SELECT value FROM COUNTRY WHERE country_id = L.country_to)
  FROM LUGGAGE L
  INNER JOIN USER_LUGGAGE U_L ON L.luggage_ID = U_L.luggage_ID
  WHERE U_L.user_ID = ID;
END //
DELIMITER ;


DROP PROCEDURE IF EXISTS get_user_luggage_offers;
DELIMITER //
CREATE PROCEDURE get_user_luggage_offers(IN ID INTEGER(10))
BEGIN
  SELECT L.create_on, 
  (SELECT value FROM COUNTRY WHERE country_id = L.country_from), 
  (SELECT value FROM COUNTRY WHERE country_id = L.country_to)
  FROM LUGGAGE L
  INNER JOIN USER_LUGGAGE U_L ON L.luggage_ID = U_L.luggage_ID
  WHERE U_L.user_ID <> ID;
END //
DELIMITER ;