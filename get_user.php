<?php
    require_once './procedures.php';
    if ($_SERVER["REQUEST_METHOD"] == "POST"){
        if(array_key_exists("luggage", $_POST)){
            $user_ID = $_POST["luggage"]["user_ID"];
            $result = get_user_luggage_offers($user_ID);
            echo json_encode($result);
        } else {
            echo json_encode(array());
        }
    }

?>