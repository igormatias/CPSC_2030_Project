DROP DATABASE IF EXISTS Project;
CREATE DATABASE Project;

USE Project;

DROP TABLE IF EXISTS USER;
CREATE TABLE USER(
    user_ID         INTEGER(10)  AUTO_INCREMENT,
    email           VARCHAR(100) NOT NULL,
    name            VARCHAR(50)  DEFAULT NULL,
    last_name       VARCHAR(50)  DEFAULT NULL,
    password        VARCHAR(128) NOT NULL,
    salt            VARCHAR(128) NOT NULL,
    birthdate       DATE         DEFAULT NULL,
    subscribe       VARCHAR(1)   DEFAULT 'N',
    create_on       TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    UNIQUE (EMAIL),
    PRIMARY KEY(user_ID)
);

DROP TABLE IF EXISTS EMAIL_CHECK;
CREATE TABLE EMAIL_CHECK(
    user_ID              INTEGER(10),
    passkey              VARCHAR(128)  NOT NULL,
    create_on            TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    checked_on           TIMESTAMP NULL,
    PRIMARY KEY(user_ID),
    FOREIGN KEY(user_ID) REFERENCES User(user_ID)
);

DROP TABLE IF EXISTS USER_ACCESS_HISTORY;
CREATE TABLE USER_ACCESS_HISTORY(
    user_ID              INTEGER(10),
    access_time          TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(user_ID, access_time),
    FOREIGN KEY(user_ID) REFERENCES User(user_ID)
);

DROP TABLE IF EXISTS COUNTRY;
CREATE TABLE COUNTRY (
    country_ID          VARCHAR(2) NOT NULL, 
    value               VARCHAR(64) NOT NULL, 
    PRIMARY KEY(country_ID)
);

DROP TABLE IF EXISTS ADDRESS;
CREATE TABLE ADDRESS(
    address_ID           INTEGER(10) AUTO_INCREMENT,
    address_name         varchar(20),
    address_1            VARCHAR(300) NOT NULL,
    address_2            VARCHAR(300),
    state                VARCHAR(100),
    city                 VARCHAR(100) NOT NULL,
    zip                  VARCHAR(20) NOT NULL,
    country_ID           VARCHAR(2) NOT NULL,
    home                 VARCHAR(1)   DEFAULT 'N',
    PRIMARY KEY(address_ID),
    FOREIGN KEY(country_ID) REFERENCES COUNTRY(country_ID)
);

DROP TABLE IF EXISTS LUGGAGE;
CREATE TABLE LUGGAGE(
    luggage_ID           INTEGER(10) AUTO_INCREMENT,
    create_on            TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    country_from         VARCHAR(2) NOT NULL,
    country_to           VARCHAR(2) NOT NULL,
    address_ID           INTEGER(10) NOT NULL,
    PRIMARY KEY(luggage_ID),
    FOREIGN KEY(country_from) REFERENCES COUNTRY(country_ID),
    FOREIGN KEY(country_to) REFERENCES COUNTRY(country_ID),
    FOREIGN KEY(address_ID) REFERENCES ADDRESS(address_ID)
);

DROP TABLE IF EXISTS USER_LUGGAGE;
CREATE TABLE USER_LUGGAGE(
    user_ID              INTEGER(10),
    luggage_ID           INTEGER(10),
    PRIMARY KEY(user_ID, luggage_ID),
    FOREIGN KEY(luggage_ID) REFERENCES LUGGAGE(luggage_ID),
    FOREIGN KEY(user_ID) REFERENCES USER(user_ID)
);

DROP TABLE IF EXISTS USER_ADDRESS;
CREATE TABLE USER_ADDRESS(
    user_ID              INTEGER(10),
    address_ID           INTEGER(10),
    PRIMARY KEY(user_ID, address_ID),
    FOREIGN KEY(address_ID) REFERENCES ADDRESS(address_ID),
    FOREIGN KEY(user_ID) REFERENCES USER(user_ID)
);

/*
CREATE TABLE Password_Recovery(
    USER_ID              INTEGER(10),
    STRING               VARCHAR(128) NOT NULL,
    GENERATED_DATETIME   DATETIME NOT NULL,
    EMAIL_CHECKED        DATETIME,
    PRIMARY KEY(USER_ID, GENERATED_DATETIME),
    FOREIGN KEY(USER_ID) REFERENCES User(ID)
);

*/