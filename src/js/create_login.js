let inputEmail = document.getElementById('InputEmail');
let inputPassword1 = document.getElementById("inputPassword1");
let inputPassword2 = document.getElementById("inputPassword2");
let passwordMatch = document.getElementById("passwordMatch");
let blankEmail = document.getElementById("blankEmail");
let blankPassword = document.getElementById("blankPassword");

passwordMatch.style.display = 'none';
blankEmail.style.display = 'none';
blankPassword.style.display = 'none';


function validate(){
    let ret = true;
    if (inputEmail.value != ""){
        blankEmail.style.display = "none";
    }
    else{
        blankEmail.style.display = "block";
        ret = false;
    }
    if (inputPassword1.value != ""){
        blankPassword.style.display = "none";
    }
    else{
        blankPassword.style.display = "block";
        ret = false;
    }
    if (inputPassword1.value == inputPassword2.value){
        passwordMatch.style.display = 'none';
    }
    else{
        passwordMatch.style.display = "block";
        ret = false;
    }
    return ret;
}