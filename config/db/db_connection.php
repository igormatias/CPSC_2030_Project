<?php
    require_once 'db_config.php';

    function connectToDatabase(){
        $conn = new mysqli(DB_SERVER,DB_USER,DB_PASSWORD,DB_DATABASE);
        if (mysqli_connect_errno())
        {
            die("unable to connect to database");
        }
        mysqli_set_charset($conn, 'utf8');                          //Unicode!
        return $conn;
    }
?>