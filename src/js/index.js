const luggageList = $('#luggageList')[0];
console.log(luggageToOffer);

for (let i = 0; i < 100; i++){
    createLuggage('brazil', 'canada', '2018-1-1');
}

function createLuggage(from, to, date) {
    let lugagge = document.createElement("div");
    $(lugagge).addClass('card');

    let link = document.createElement("a");
    $(link).attr('href', '#');
    $(link).addClass('luggage-link');
    $(link).appendTo(lugagge);

    let img = document.createElement("img");
    $(img).attr('src', './src/assets/img/luggage.png');
    $(img).addClass('card-img-top');
    $(img).appendTo(link);

    let luggageBody = document.createElement("div");
    $(luggageBody).addClass('card-body');
    $(luggageBody).appendTo(link);


    let title = document.createElement("h5");
    $(title).addClass('card-title');
    $(title).text(from + ' - ' +  to);
    $(title).appendTo(luggageBody);

    let text = document.createElement("p");
    $(text).addClass('card-text');
    $(text).text(date)
    $(text).appendTo(luggageBody);

    $(lugagge).appendTo(luggageList);
}
