<?php
    require_once './procedures.php';
    if ($_SERVER["REQUEST_METHOD"] == "POST"){
        if(array_key_exists("address", $_POST)){
            if (create_new_user_address($_POST['address']['user_ID'], $_POST['address']['new_address']));
                echo json_encode($answer = 'ok');
        } else {
            echo json_encode(array());
        }
        if(array_key_exists("luggage", $_POST)){
            if (create_new_user_luggage($_POST['luggage']['user_ID'], $_POST['luggage']['new_luggage']))
                echo json_encode($answer = 'ok');
        } else {
            echo json_encode(array());
        }

    }

?>