<?php
    require_once './procedures.php';

    session_start();

    if (is_valid_session()){
        $session = get_session();
        $user['user_ID'] = $session['user_ID'];
        $user['profile']['email'] = $session['email'];
        $menu_top['login']['link'] = 'index.php?action=logout';
        $menu_top['login']['label'] = 'logout';
        $menu_top['loginButton'] = 'menu_top.user.html.twig';   
        $index['sidemenu'] = 'luggage.sidemenu.html.twig';
        $index['body'] = 'luggage.list.html.twig';
        $index['main'] = 'index.side_menu.twig';
        //print_r(get_session());
    }

    if (isset($_GET['action'])) {
        $index['body'] = $_GET['action'];
        $index['body'] = $index['body']. '.html.twig';
        if ($_GET['action'] == 'logout'){
            $index['main'] = 'index.no_side_menu.twig';
            $menu_top['loginButton'] = 'menu_top.login.html.twig';
            $menu_top['login']['link'] = 'index.php?action=login';
            $menu_top['login']['label'] = 'login';
            $menu_top['login']['form'] = 'login.html.twig';    
            clear_session();
        }
        else if (strpos ($_GET['action'], 'user.profile') !== FALSE){
            $menu_top['login']['link'] = 'index.php?action=logout';
            $menu_top['login']['label'] = 'logout';
            $menu_top['loginButton'] = 'menu_top.user.html.twig';
            $index['main'] = 'index.side_menu.twig';
            $index['sidemenu'] = 'user.sidemenu.html.twig';
        }
        else{

        }
    }

    if (isset($_GET['emailcheck'])) {
        //echo file_get_contents('php://input');
        $emailCheck = $_GET['emailcheck'];
        $token =  $_GET['token'];
        $email =  check_passkey($token);
        if (!isset($user['checked_on'])){
            $user['profile']['email'] = $email['email'];
            set_session($user, $email);
            $index['body']  = 'email_verified.html.twig';    
        }
    }
    require_once './twig.php';

?>