<?php
    require_once './vendor/autoload.php';

    $createSelectBoxCountryList = new Twig_Filter('createSelectBoxCountryList', 'createSelectBoxCountryList');
    $createSelectBoxAddressList = new Twig_Filter('createSelectBoxAddressList', 'createSelectBoxAddressList');

    $loader = new Twig_Loader_Filesystem('./templates');
    //$twig = new Twig_Environment($loader, array('cache' => '/path/to/compilation_cache',));
    $twig = new Twig_Environment($loader);
    
    $twig->addFilter($createSelectBoxCountryList);
    $twig->addFilter($createSelectBoxAddressList);

    echo $twig->render('index.twig', array(     'index' =>  $index,
                                                'menu_top' => $menu_top,
                                                //'logon' => $logon,
                                                //'body' => $body,
                                                //'main' => $main,
                                                'user' => $user,
                                                'title' => 'Lugrent',
                                                'fname' => '',
                                                'lname' => ''));

?>