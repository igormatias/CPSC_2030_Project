<?php
    require_once './procedures.php';
    
    session_start();
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if (isset($_POST['email'])){
            $email = $_POST['email'];
            $password =  $_POST['password'];
            $subscribe = $_POST['subscribe'];
            $user =  create_new_user($email, $password, $subscribe);
            if ($user != NULL){
                $user['profile']['email'] = $email;
                $index['body'] = 'email_sent.html.twig';
                send_email($user, $email);
    
            }
            else{
                $index['body'] = 'email_not_sent.html.twig';
            }    
        }
        if (isset($_POST['userEmail'])){
            $email = $_POST['userEmail'];
            $password =  $_POST['userPassword'];
            if (!isset($_POST['keep']))
                $keep = null;
            else
                $keep = $_POST['keep'];
            $user = login($email, $password);
            if ($user != NULL){
                if (email_checked($user) == 'Y'){
                    set_session($user, $email, $keep);
                    header("Location: index.php");
                }
                else{
                    $index['body'] = 'email_sent.html.twig';
                }
            }
            else{
                $index['body'] = 'login_fail.html.twig';
            }
        }
    }

    require_once './twig.php';
?>